﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using Telephone_Interface.Models;

namespace Telephone_Interface.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult StartCall()
        {
            // ----------------------------------------
            // This procedure just start the main menu
            // ----------------------------------------

            // Gets a connection Vairable ready
            ConnectionRepository cr = new ConnectionRepository();

            // Requests a session token from the Middle Tier using the calling Telephone Number  
            List<object> oSessionToken = cr.request_token(Request["From"]);

            // Check to see if it managed to get a session token
            if (Convert.ToBoolean(oSessionToken[0].ToString()) == false)
            {
                // if it failed to get a session token because the telephone number didnt exist on the database or is disabled
                if (oSessionToken[1].ToString().Contains("Login device failed"))
                {
                    return View("Call_DeviceNotAuthorized");
                }

                // if it failed to get a session token because of any other reason
                return View("Call_Error");
            }
            else
            {
                // Check to see if the telephone is connected to a site
                TelephoneRepository tr = new TelephoneRepository();
                String sSite = tr.get_telephone_details(Request["From"].ToString(), oSessionToken[1].ToString());

                if (sSite == null)
                {
                    return View("Call_DeviceNotAuthorized");
                }
            }

            return View();
        }

        public ActionResult Call_Options(string Digits)
        {
            // ----------------------------------------
            // This procedure is when the employee has entered their option on the main menu
            // ----------------------------------------

            if (string.IsNullOrEmpty(Digits))
            {
                Digits = "";   
            } 

            // User has pressed 1 for Reocrding their attendance
            if (Digits == "1")
            {
                return View("Call_Options_ClockIn");
            }

            if (Digits == "2")
            {
                return View("Call_Options_Schedules_GetEmployee");
            }
            
            // if another digit was entered then take the call back to the start
            return View("StartCall");
        }

        public ActionResult Call_RecordAttendance(string Digits)
        {
            // ----------------------------------------
            // This procedure is for then the employee has entered their maxtime code to clock in
            // ----------------------------------------

            TelephoneRepository tr = new TelephoneRepository();
            ConnectionRepository cr = new ConnectionRepository();
            Dictionary<String, String> oEmployeeDetails = new Dictionary<String, String>();

            // Requests a session token from the Middle Tier using the calling Telephone Number  
            List<object> oSessionToken = cr.request_token(Request["From"]);

            // if it fails to get the session token get them to enter the employee code again
            if (Convert.ToBoolean(oSessionToken[0].ToString()) == false)
            {
                ViewBag.ExtraMessage = "There was an error getting your details";
                return View("Call_Options_ClockIn");
            }
            else
            {
                // Gets the employee details
                oEmployeeDetails = tr.get_employee_details(Digits, oSessionToken[1].ToString());

                // If it couldnt get details for the employee get them to enter the code again
                if (oEmployeeDetails == null)
                {
                    ViewBag.ExtraMessage = "You have entered an invalid employee code";
                    return View("Call_Options_ClockIn");
                }
            }


            return View("Call_RecordAttendance", oEmployeeDetails);
        }

        public ActionResult Call_Options_Schedules_ConfirmEmployee(string Digits)
        {
            // ----------------------------------------
            // This procedure is for then the employee has entered their maxtime code to clock in
            // ----------------------------------------

            TelephoneRepository tr = new TelephoneRepository();
            ConnectionRepository cr = new ConnectionRepository();
            Dictionary<String, String> oEmployeeDetails = new Dictionary<String, String>();

            // Requests a session token from the Middle Tier using the calling Telephone Number  
            List<object> oSessionToken = cr.request_token(Request["From"]);

            // if it fails to get the session token get them to enter the employee code again
            if (Convert.ToBoolean(oSessionToken[0].ToString()) == false)
            {
                ViewBag.ExtraMessage = "There was an error getting your details";
                return View("Call_Options_Schedules_GetEmployee");
            }
            else
            {
                // Gets the employee details
                oEmployeeDetails = tr.get_employee_details(Digits, oSessionToken[1].ToString());

                // If it couldnt get details for the employee get them to enter the code again
                if (oEmployeeDetails == null)
                {
                    ViewBag.ExtraMessage = "You have entered an invalid employee code";
                    return View("Call_Options_Schedules_GetEmployee");
                }
            }


            return View("Call_Options_Schedules_ConfirmEmployee", oEmployeeDetails);
        }


        public ActionResult Call_RecordAttendance_Option(string EmpCode, string Digits)
        {
            // ----------------------------------------
            // This procedure is when the employee has confirmed if the employee found is them
            // ----------------------------------------

            TelephoneRepository tr = new TelephoneRepository();
            ConnectionRepository cr = new ConnectionRepository();
            String sPunchResults = "";


            if (Digits == "1")
            {
                List<object> oSessionToken = cr.request_token(Request["From"]);

                // if it fails to get the session token get them to enter the employee code again
                if (Convert.ToBoolean(oSessionToken[0].ToString()) == false)
                {
                    ViewBag.ExtraMessage = "There was an error recording your attendance";
                    return View("Call_Options_ClockIn");
                }
                else
                {
                    sPunchResults = tr.post_punch(EmpCode, Request["From"].ToString(), oSessionToken[1].ToString());
                    if (sPunchResults == null)
                    {
                        ViewBag.ExtraMessage = "There was an error recording your attendance";
                        return View("Call_Options_ClockIn");
                    }
                    tr.process_punches(oSessionToken[1].ToString(), EmpCode);

                    ViewBag.PunchTime = sPunchResults;
                    return View("Call_RecordAttendance_Option_Success");
                }
            }
            else
            {
                ViewBag.ExtraMessage = "";
                return View("Call_Options_ClockIn");
            }
        }

        public ActionResult Call_GetSchedules(string EmpCode, string Digits)
        {
            // ----------------------------------------
            // This procedure is for the employees schedules
            // ----------------------------------------

            if (Digits == "1")
            {
                TelephoneRepository tr = new TelephoneRepository();
                ConnectionRepository cr = new ConnectionRepository();
                List<Dictionary<String, String>> oSchedules = new List<Dictionary<String, String>>();

                // Requests a session token from the Middle Tier using the calling Telephone Number  
                List<object> oSessionToken = cr.request_token(Request["From"]);
                ViewBag.EmployeeCode = Digits;
                // if it fails to get the session token get them to enter the employee code again
                if (Convert.ToBoolean(oSessionToken[0].ToString()) == false)
                {
                    ViewBag.ExtraMessage = "There was an error getting your details";
                    return View("Call_Options_Schedules_GetEmployee");
                }
                else
                {
                    // Gets the employee details
                    oSchedules = tr.get_schedules(EmpCode, oSessionToken[1].ToString());

                    // If it couldnt get details for the employee get them to enter the code again
                    if (oSchedules == null)
                    {
                        oSchedules = new List<Dictionary<String, String>>();
                    }
                }


                return View("Call_Options_Schedules", oSchedules);
            }
            else
            {
                return View("Call_Options_Schedules_GetEmployee");
            }
        }


        public ActionResult Call_Option_Schedules_Again(string Digits, String EmpCode)
        {
            // ----------------------------------------
            // 
            // ----------------------------------------
            if(Digits == "1")
            {
                return Call_GetSchedules(EmpCode,"1");
            }

            return View("StartCall");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using Newtonsoft.Json;

namespace Telephone_Interface.Models
{
    public class ConnectionRepository
    {

        // Local
        //public const String AUTHURL = @"http://auth.maxtime.co.uk/v1.3/";
        //public const String APIURL = @"http://160.1.0.106:54572/api/";
        //public const String INTERFACEURL = @"http://ldsoffice.co.uk:63653/";

        // Live
        public const String AUTHURL = @"http://auth.maxtime.co.uk/v1.3/";
        public const String APIURL = @"http://telapi.maxtime.co.uk/v1.0/api/";
        public const String INTERFACEURL = @"http://tel.maxtime.co.uk/";

        public const String APPTYPE = "Telephone Interface";
        public const String APPMAJOR = "1";
        public const String APPMINOR = "0";
        public const String APINAME = "telephone api";
        public const String APIMAJOR = "1";
        public const String APIMINOR = "0";
        public const String error_email = "development@ldssystems.co.uk";
        public const String error_pass = "$sonic47235!";

        public List<object> request_token(String SN)
        {
            using (HttpClient hc = new HttpClient())
            {
                // This procedure gets a vaild token 
                HttpStatusCode hsc = new HttpStatusCode();
                List<object> o = new List<object>();
                o.Add(false);
                o.Add("Start");
                String session_token = null;
                Dictionary<String, String> prms = new Dictionary<String, String>();
                try
                {
                    // Creates a request to log in the device i.e the Phone
                    HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Post, AUTHURL + @"user/login_device");
                    req.Content = new StringContent(JsonConvert.SerializeObject(SN), Encoding.UTF8, "application/json");
                    HttpResponseMessage result = hc.SendAsync(req).Result;
                    hsc = result.StatusCode;
                    if (hsc != HttpStatusCode.OK) throw new Exception("Login device failed", new Exception(result.Content.ReadAsStringAsync().Result));
                    List<Dictionary<String, String>> d = JsonConvert.DeserializeObject<List<Dictionary<String, String>>>(result.Content.ReadAsStringAsync().Result);
                    session_token = d[0]["token"];
                    o[1] = (session_token);

                    if (session_token == "MULTIPLE" || session_token == "INCORRECT")
                    {
                        o[1] = "Login device failed: " + session_token + ". Is this Device Active?";
                        return o;
                    }


                    // Creates a request to set the application im using
                    req = new HttpRequestMessage(HttpMethod.Post, AUTHURL + @"session/set_application");
                    req.Headers.Add("session_token", session_token);
                    prms = new Dictionary<String, String>() 
                        { 
                            { "name", APPTYPE },
                            { "major", APPMAJOR },
                            { "minor", APPMINOR }                         
                        };
                    req.Content = new StringContent(JsonConvert.SerializeObject(prms), Encoding.UTF8, "application/json");
                    result = hc.SendAsync(req).Result;
                    hsc = result.StatusCode;
                    if (hsc != HttpStatusCode.OK) throw new Exception("Set Application failed for this session", new Exception(result.Content.ReadAsStringAsync().Result));
                    String recordsAffected = JsonConvert.DeserializeObject<String>(result.Content.ReadAsStringAsync().Result);
                    if (hsc != HttpStatusCode.OK) throw new Exception("Failed to set the application for this session", new Exception(recordsAffected, new Exception(result.Content.ReadAsStringAsync().Result)));
                    if (recordsAffected != "1")
                    {
                        o[1] = "Failed to set the application for this session, Are there active applications for this device?";
                        return o;
                    }

                    req = new HttpRequestMessage(HttpMethod.Get, AUTHURL + @"session/list_database");
                    req.Headers.Add("session_token", session_token);
                    result = hc.SendAsync(req).Result;
                    hsc = result.StatusCode;
                    if (hsc == HttpStatusCode.InternalServerError) throw new Exception("List Database failed for this session", new Exception(result.Content.ReadAsStringAsync().Result));
                    if (result.Content.ReadAsStringAsync().Result.ToLower().Contains("no result found"))
                    {
                        o[1] = "There are no available databases for this session, Is the database active for this device?";
                        return o;
                    }
                    d = JsonConvert.DeserializeObject<List<Dictionary<String, String>>>(result.Content.ReadAsStringAsync().Result);
                    Dictionary<String, String> db = d[0];

                    req = new HttpRequestMessage(HttpMethod.Post, AUTHURL + @"session/set_database");
                    req.Headers.Add("session_token", session_token);
                    req.Content = new StringContent(JsonConvert.SerializeObject(db), Encoding.UTF8, "application/json");
                    result = hc.SendAsync(req).Result;
                    hsc = result.StatusCode;
                    if (hsc != HttpStatusCode.OK) throw new Exception("Set Database failed for this session", new Exception(result.Content.ReadAsStringAsync().Result));
                    recordsAffected = JsonConvert.DeserializeObject<String>(result.Content.ReadAsStringAsync().Result);
                    if (recordsAffected != "1")
                    {
                        o[1] = "Failed to set the database for this session, Is the database active for this device?";
                        return o;
                    }

                    req = new HttpRequestMessage(HttpMethod.Post, AUTHURL + @"session/set_api");
                    req.Headers.Add("session_token", session_token);
                    prms = new Dictionary<String, String>() 
                        { 
                            { "name", APINAME },
                            { "major", APIMAJOR },
                            { "minor", APIMINOR }                         
                        };
                    req.Content = new StringContent(JsonConvert.SerializeObject(prms), Encoding.UTF8, "application/json");
                    result = hc.SendAsync(req).Result;
                    hsc = result.StatusCode;
                    if (hsc != HttpStatusCode.OK) throw new Exception("Set Api failed for this session", new Exception(result.Content.ReadAsStringAsync().Result));
                    recordsAffected = JsonConvert.DeserializeObject<String>(result.Content.ReadAsStringAsync().Result);
                    //Unlike the others this needs to have two records affected as there is a multi table update for both session and device
                    if (recordsAffected != "2")
                    {
                        o[1] = "Failed to set the api for this session, Is the api active for this device?";
                        return o;
                    }

                    //DONE
                    o[0] = true;
                    return o;
                }
                catch (Exception e)
                {
                    //Action failure, attempt to send email
                    String errorMessages = maxtime_functions.GetExceptionMessages(e);
                    try
                    {
                        if (errorMessages.Contains("Unable to connect to the remote server") || hsc == HttpStatusCode.BadRequest) return o;
                        Dictionary<String, Object> error = new Dictionary<String, Object>();
                        error.Add("Source", "Telephone Interface");
                        error.Add("Parameters", new List<Object>() { session_token, SN, prms });
                        maxtime_functions.sendEmail("request_token", errorMessages + Environment.NewLine + Environment.NewLine + JsonConvert.SerializeObject(error));
                    }
                    catch (Exception ex)
                    {
                        //Failed to send email.
                    }
                    return o;
                }
            }
        }
    }
}
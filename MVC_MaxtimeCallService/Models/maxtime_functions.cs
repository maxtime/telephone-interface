﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Web;

namespace Telephone_Interface.Models
{
    public static class maxtime_functions
    {
        public static void sendEmail(String Subject, String Body)
        {
            MailMessage mail = new MailMessage();

            SmtpClient smtp = new SmtpClient("smtp.gmail.com");
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential(ConnectionRepository.error_email, ConnectionRepository.error_pass);
            smtp.Port = 587;
            smtp.EnableSsl = true;

            mail.From = new MailAddress(ConnectionRepository.error_email, "Telephone Interface");
            mail.To.Add("lee.thomas@ldssystems.co.uk");
            mail.CC.Add("chad.lowe@ldssystems.co.uk");
            mail.Subject = Subject;
            mail.Body = Body;

            smtp.Send(mail);
        }

        public static String GetExceptionMessages(this Exception e, String messages = "")
        {
            if (e == null) return String.Empty;
            if (messages == "") messages = e.Message;
            if (e.InnerException != null)
            {
                messages += Environment.NewLine + "Inner Exception: " + GetExceptionMessages(e.InnerException);
            }
            return messages;
        }

        public static List<String> captureHTTPContent(HttpContent rc)
        {
            List<String> content = Regex.Split(rc.ReadAsStringAsync().Result, "\n").ToList();
            return content;
        }

        public static byte[] captureStreamContent(HttpContent rc)
        {
            byte[] content = rc.ReadAsByteArrayAsync().Result;
            return content;
        }

        public static Int32 ConvertToInt32(Int16 highWord, Int16 lowWord)
        {
            Int32 employee = (highWord * 0x10000) + lowWord;
            return employee;
        }

        public static string DecimalToReversedHex(string oValue)
        {
            int nValue = Convert.ToInt32(oValue);
            // Convert integer as a hex in a string variable
            string hexValue = nValue.ToString("X").PadLeft(10, '0');
            string sReversed = "";
            //Start from the end of the string going back in 2's
            for (int nLoop = 4; nLoop >= 0; nLoop--)
            {
                sReversed += hexValue[2 * nLoop];
                sReversed += hexValue[(2 * nLoop) + 1];
            }
            return sReversed;
        }

        public static string ReversedHexToDecimal(string oValue)
        {
            string sReversed = "";
            //Start from the end of the string going back in 2's
            for (int nLoop = 4; nLoop >= 0; nLoop--)
            {
                sReversed += oValue[2 * nLoop];
                sReversed += oValue[(2 * nLoop) + 1];
            }

            uint num = uint.Parse(sReversed, System.Globalization.NumberStyles.AllowHexSpecifier);
            return num.ToString().PadLeft(10, '0');
        }

        public static String getDateHeader()
        {
            string WeekDay = DateTime.UtcNow.DayOfWeek.ToString().Remove(3); // English

            string Month = DateTime.UtcNow.Month.ToString();

            if (Month == "1") Month = "Jan"; // English
            if (Month == "2") Month = "Feb";
            if (Month == "3") Month = "Mar";
            if (Month == "4") Month = "Apr";
            if (Month == "5") Month = "May";
            if (Month == "6") Month = "Jun";
            if (Month == "7") Month = "Jul";
            if (Month == "8") Month = "Aug";
            if (Month == "9") Month = "Sep";
            if (Month == "10") Month = "Oct";
            if (Month == "11") Month = "Nov";
            if (Month == "12") Month = "Dec";

            string Hour = DateTime.UtcNow.AddHours(1).Hour.ToString();
            string Minute = DateTime.UtcNow.Minute.ToString();
            string Second = DateTime.UtcNow.Second.ToString();

            if (Hour.Length == 1) Hour = "0" + Hour;
            if (Minute.Length == 1) Minute = "0" + Minute;
            if (Second.Length == 1) Second = "0" + Second;

            String dateHeader = WeekDay + ", " + DateTime.UtcNow.Day + " " + Month + " " + DateTime.UtcNow.Year + " " + Hour + ":" + Minute + ":" + Second + " GMT";
            return dateHeader;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using Newtonsoft.Json;

namespace Telephone_Interface.Models
{
    public class TelephoneRepository
    {
        public String get_telephone_details(String TelephoneNumber, String session_token)
        {
            using (HttpClient hc = new HttpClient())
            {
                HttpStatusCode hsc = new HttpStatusCode();
                List<Dictionary<String, String>> d = new List<Dictionary<String, String>>();
                try
                {
                    HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Post, ConnectionRepository.APIURL + @"home/get_telephone_details");
                    req.Headers.Add("session_token", session_token);
                    req.Headers.Add("app_name", ConnectionRepository.APPTYPE);
                    req.Headers.Add("app_major", ConnectionRepository.APPMAJOR);
                    req.Headers.Add("app_minor", ConnectionRepository.APPMINOR);

                    req.Content = new StringContent(JsonConvert.SerializeObject(TelephoneNumber), Encoding.UTF8, "application/json");
                    HttpResponseMessage result = hc.SendAsync(req).Result;
                    hsc = result.StatusCode;
                    if (hsc != HttpStatusCode.OK) throw new Exception("get_telephone_details failed", new Exception(result.Content.ReadAsStringAsync().Result));
                    d = JsonConvert.DeserializeObject<List<Dictionary<String, String>>>(result.Content.ReadAsStringAsync().Result);
                    if (!d[0].ContainsKey("Code"))
                    {
                        return "";
                    }
                    else
                    {
                        return d[0].ToString();
                    }
                    
                }
                catch (Exception e)
                {
                    //Action failure, attempt to send email
                    String errorMessages = maxtime_functions.GetExceptionMessages(e);
                    try
                    {
                        if (errorMessages.Contains("Unable to connect to the remote server") || hsc == HttpStatusCode.BadRequest || hsc == HttpStatusCode.BadRequest) return null;
                        Dictionary<String, Object> error = new Dictionary<String, Object>();
                        error.Add("Source", "Telephone Interface");
                        error.Add("Parameters", new List<Object>() { session_token, TelephoneNumber });
                        maxtime_functions.sendEmail("get_telephone_details failed", errorMessages + Environment.NewLine + Environment.NewLine + JsonConvert.SerializeObject(error));
                    }
                    catch (Exception ex)
                    {
                        //Failed to send email.
                    }
                    return null;
                }
            }
        }

        public Dictionary<string, string> get_employee_details(String Employee_Code, String session_token)
        {
            using(HttpClient hc = new HttpClient())
            {
                HttpStatusCode hsc = new HttpStatusCode();
                List<Dictionary<String, String>> d = new List<Dictionary<String, String>>();
                try
                {
                    HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Post, ConnectionRepository.APIURL + @"home/get_employee_details");
                    req.Headers.Add("session_token", session_token);
                    req.Headers.Add("app_name", ConnectionRepository.APPTYPE);
                    req.Headers.Add("app_major", ConnectionRepository.APPMAJOR);
                    req.Headers.Add("app_minor", ConnectionRepository.APPMINOR);

                    req.Content = new StringContent(JsonConvert.SerializeObject(Employee_Code), Encoding.UTF8, "application/json");
                    HttpResponseMessage result = hc.SendAsync(req).Result;
                    hsc = result.StatusCode;
                    if (hsc != HttpStatusCode.OK) throw new Exception("get_clock_code failed", new Exception(result.Content.ReadAsStringAsync().Result));
                    d = JsonConvert.DeserializeObject<List<Dictionary<String, String>>>(result.Content.ReadAsStringAsync().Result);
                    if (!d[0].ContainsKey("Forename") && d[0]["Forename"] != "") throw new Exception("Clock Code not retrieved, get_clock_code failed ", new Exception(result.Content.ReadAsStringAsync().Result));
                    return d[0];
                }
                catch (Exception e)
                {
                    //Action failure, attempt to send email
                    String errorMessages = maxtime_functions.GetExceptionMessages(e);
                    try
                    {
                        if (errorMessages.Contains("Unable to connect to the remote server") || hsc == HttpStatusCode.BadRequest || hsc == HttpStatusCode.BadRequest) return null;
                        Dictionary<String, Object> error = new Dictionary<String, Object>();
                        error.Add("Source", "Telephone Interface");
                        error.Add("Parameters", new List<Object>() { session_token, Employee_Code });
                        maxtime_functions.sendEmail("get_employee_details failed", errorMessages + Environment.NewLine + Environment.NewLine + JsonConvert.SerializeObject(error));
                    }
                    catch (Exception ex)
                    {
                        //Failed to send email.
                    }
                    return null;
                }
            }
        }

        public String post_punch(String Employee_Code, String TelephoneNumber, String session_token)
        {
            DateTime oPunchDateTime = DateTime.Now;
            using (HttpClient hc = new HttpClient())
            {
                HttpStatusCode hsc = new HttpStatusCode();
                List<Dictionary<String, String>> d = new List<Dictionary<String, String>>();
                try
                {
                    HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Post, ConnectionRepository.APIURL + @"home/post_punch");
                    req.Headers.Add("session_token", session_token);
                    req.Headers.Add("app_name", ConnectionRepository.APPTYPE);
                    req.Headers.Add("app_major", ConnectionRepository.APPMAJOR);
                    req.Headers.Add("app_minor", ConnectionRepository.APPMINOR);

                    Dictionary<String, String> oParams = new Dictionary<string, string>();

                    oParams.Add("employee_code", Employee_Code);
                    oParams.Add("telephonenumber", TelephoneNumber);
                    oParams.Add("punchtime", oPunchDateTime.ToString("yyyy-MM-dd HH:mm:ss"));

                    req.Content = new StringContent(JsonConvert.SerializeObject(oParams), Encoding.UTF8, "application/json");
                    HttpResponseMessage result = hc.SendAsync(req).Result;
                    hsc = result.StatusCode;
                    if (hsc != HttpStatusCode.OK)
                    {
                        throw new Exception("post_punch failed", new Exception(result.Content.ReadAsStringAsync().Result));
                    }else
                    {
                        return oPunchDateTime.ToString("hh:mm tt");
                    }
                }
                catch (Exception e)
                {
                    //Action failure, attempt to send email
                    String errorMessages = maxtime_functions.GetExceptionMessages(e);
                    try
                    {
                        if (errorMessages.Contains("Unable to connect to the remote server") || hsc == HttpStatusCode.BadRequest || hsc == HttpStatusCode.BadRequest) return null;
                        Dictionary<String, Object> error = new Dictionary<String, Object>();
                        error.Add("Source", "Telephone Interface");
                        error.Add("Parameters", new List<Object>() { session_token, Employee_Code });
                        maxtime_functions.sendEmail("post_punch failed", errorMessages + Environment.NewLine + Environment.NewLine + JsonConvert.SerializeObject(error));
                    }
                    catch (Exception ex)
                    {
                        //Failed to send email.
                    }
                    return null;
                }
            }
        }

        public void process_punches(String session_token, String Employee_Code)
        {
            using (HttpClient hc = new HttpClient())
            {
                HttpStatusCode hsc = new HttpStatusCode();
                try
                {
                    HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Post, ConnectionRepository.APIURL + @"home/process_punches");
                    req.Headers.Add("session_token", session_token);
                    req.Headers.Add("app_name", ConnectionRepository.APPTYPE);
                    req.Headers.Add("app_major", ConnectionRepository.APPMAJOR);
                    req.Headers.Add("app_minor", ConnectionRepository.APPMINOR);
                    req.Content = new StringContent(JsonConvert.SerializeObject(Employee_Code), Encoding.UTF8, "application/json");
                    HttpResponseMessage result = hc.SendAsync(req).Result;
                    hsc = result.StatusCode;
                    if (hsc != HttpStatusCode.OK) throw new Exception("process_punches failed", new Exception(result.Content.ReadAsStringAsync().Result));
                }
                catch (Exception e)
                {
                    //Action failure, attempt to send email
                    String errorMessages = maxtime_functions.GetExceptionMessages(e);
                    try
                    {
                        if (errorMessages.Contains("Unable to connect to the remote server") || hsc == HttpStatusCode.BadRequest) return;
                        Dictionary<String, Object> error = new Dictionary<String, Object>();
                        error.Add("Source", "Telephone Interface");
                        error.Add("Parameters", new List<Object>() { session_token });
                        maxtime_functions.sendEmail("process_punches failed", errorMessages + Environment.NewLine + Environment.NewLine + JsonConvert.SerializeObject(error));
                    }
                    catch (Exception ex)
                    {
                        //Failed to send email.
                    }
                }
            }
        }

        public List<Dictionary<String,String>> get_schedules(String Employee_Code, String session_token)
        {
            using (HttpClient hc = new HttpClient())
            {
                HttpStatusCode hsc = new HttpStatusCode();
                List<Dictionary<String, String>> d = new List<Dictionary<String, String>>();
                try
                {
                    HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Post, ConnectionRepository.APIURL + @"home/get_todays_schedules");
                    req.Headers.Add("session_token", session_token);
                    req.Headers.Add("app_name", ConnectionRepository.APPTYPE);
                    req.Headers.Add("app_major", ConnectionRepository.APPMAJOR);
                    req.Headers.Add("app_minor", ConnectionRepository.APPMINOR);

                    req.Content = new StringContent(JsonConvert.SerializeObject(Employee_Code), Encoding.UTF8, "application/json");
                    HttpResponseMessage result = hc.SendAsync(req).Result;
                    hsc = result.StatusCode;
                    if (hsc != HttpStatusCode.OK) throw new Exception("get_schedules failed", new Exception(result.Content.ReadAsStringAsync().Result));
                    d = JsonConvert.DeserializeObject<List<Dictionary<String, String>>>(result.Content.ReadAsStringAsync().Result);

                    return d;

                }
                catch (Exception e)
                {
                    //Action failure, attempt to send email
                    String errorMessages = maxtime_functions.GetExceptionMessages(e);
                    try
                    {
                        if (errorMessages.Contains("Unable to connect to the remote server") || hsc == HttpStatusCode.BadRequest || hsc == HttpStatusCode.BadRequest) return null;
                        Dictionary<String, Object> error = new Dictionary<String, Object>();
                        error.Add("Source", "Telephone Interface");
                        error.Add("Parameters", new List<Object>() { session_token, Employee_Code });
                        maxtime_functions.sendEmail("get_schedules failed", errorMessages + Environment.NewLine + Environment.NewLine + JsonConvert.SerializeObject(error));
                    }
                    catch (Exception ex)
                    {
                        //Failed to send email.
                    }
                    return null;
                }
            }
        }
    }
}